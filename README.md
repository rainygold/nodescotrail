# Build
* Terminal command: 'node index.js'
* Testing: 'npm test'
* Local docker build required from 'https://github.com/rtgnx/scotrail-api'
* Database: command - 'mongod' / site - 'localhost/ScotRailJourneys'
* Steps completed locally to run project: mongod -> docker start scotrail-api -> node index.js
* DB url / specific station e.g. Bathgate - can be changed via a variable value

# Tools
* OS: Linux (with kernel 5.0.12)
* Editors: Visual Studio Code / Emacs
* Nodejs: 10.15.3
* Testing: Mocha
* Docker

# Ethos
A CLI program that prints the trains/journeys that start at Glasgow Queen Street, will stop at Bathgate station and end at Edinburgh Waverley. The printed information includes the id, platform, destination, departure time, arrival time, expected time, origin station, and service operator.

Unable to access the API via the public endpoints e.g. 'https://scotrail.pw/live/GLQ' so the API project has to be built locally and run via Docker.

# Testing
All testing was written using the 'should' and 'mocha' dependencies.

# Improvements
* More extensive testing with negative testing
* Further usage of standard built-in functional methods (e.g. map, filter) 