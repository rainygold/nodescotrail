// for export
var methods = {};

// imports and requires
var request = require('request-promise');

// returns json from the API 
methods.getJson = async function GETJsonFromAPI() {

    // easily changed config 
    const options = {
        uri: 'http://localhost:8981/live/GLQ',
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true
    };

    // use a promise as the request can be delayed
    let promise = new Promise((resolve, reject) => {

        request(options)
            .then(function (json) {
                resolve(json);
            })
            .catch(function (err) {
                reject(err);
            });

    });

    return await promise;
}

// returns json for a specific service from the API
methods.getServiceJson = async function GETServiceJsonFromAPI(serviceId) {

    // easily changed config
    const options = {
        uri: 'http://localhost:8981/service/' + serviceId,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true
    };

    // use a promise as the request can be delayed
    let promise = new Promise((resolve, reject) => {

        request(options)
            .then(function (json) {
                resolve(json);
            })
            .catch(function (err) {
                reject(err);
            });
    });

    return await promise;
}

exports.data = methods;