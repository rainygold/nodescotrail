// imports and requires
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost/ScotRailJourneys';
const options = {
    useNewUrlParser: true,
}

var methods = {};

// returns all data stored in the db
methods.retrieveData = function retrieveDataFromDb() {

    MongoClient.connect(url, options, function (err, db) {

        // throw if it is a connection problem
        if (err) throw err;

        var cursor = db.db().collection('Trains').find();

        cursor.forEach(function (doc) {
            console.log(doc);
        });

        db.close();
    });
}

// deletes all data stored in the db
methods.deleteData = function deleteAllData() {

    MongoClient.connect(url, options, function (err, db) {

        // throw if it is a connection problem
        if (err) throw err;

        try {
            db.db().collection('Trains').deleteMany();
        } catch (error) {
            throw error;
        } finally {
            db.close();
        }
    });
}

// creates the db document
methods.createCollection = function createCollectionInDb() {

    MongoClient.connect(url, options, function (err, db) {

        // throw if it is a connection problem
        if (err) throw err;

        db.db().createCollection('Trains');
        db.db().collection('Trains').createIndex({ id: 1 }, { unique: true });
        db.close();
    })
}

// inserts a record in the db
methods.insertData = function insertDataIntoDb(data) {

    MongoClient.connect(url, options, function (err, db) {

        // throw if it is a connection problem
        if (err) throw err;

        // inserts the input parameter
        try {
            db.db().collection('Trains').insertOne({
                id: data.id,
                platform: data.platform,
                destination: data.destination,
                departs: data.departs,
                arrives: data.arrives,
                expected: data.expected,
                origin: data.origin,
                operator: data.operator
            })
        } catch (error) {
            // the error will just be a duplicate key
        } finally {
            db.close();
        }
    });
}

// checks if the record exists
methods.checkData = function checkIfDataDuplicate(data) {

    MongoClient.connect(url, options, function (err, db) {

        // throw if it is a connection problem
        if (err) throw err;

        // counts the number of documents matching the predicate
        var cursor = db.db().collection('Trains');
        cursor.find({ id: data.id }).count().then(function (result) {
            db.close();
            return result;
        })


    });
}



exports.data = methods;