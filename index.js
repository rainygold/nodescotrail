// imports and requires
const api = require('./api');
const mongo = require('./db');

var methods = {};

// filters JSON for Edinburgh Waverley destination
methods.filterJSON = function filterJSON(json) {

    const filteredJson = json.services.filter(function (train) {

        // only trips that end at Waverley
        return train.destination == 'Edinburgh';
    });

    return filteredJson;
}

// filters JSON for Bathgate stop(s)
methods.filterJSONBathgate = function filterJSONBathgate(firstFilteredJson) {

    firstFilteredJson.forEach(element => {

        const serviceJsonInAPromise = api.data.getServiceJson(element.id);

        serviceJsonInAPromise.then(function (result) {

            result.forEach(secondElement => {

                // check that the entry doesn't already exist in the db && it stops at a certain station
                if (secondElement.Station == 'Bathgate') {
                    mongo.data.insertData(element);
                }
            });
        }).catch(function (err) {

            // ignore service not found errors
            if (errr.statusCode != 406) {
                console.log("Error during filterJSONBathgate: " + err);
            }
        });
    });
}

// print test for JSON
const requestJson = api.data.getJson();
requestJson.then(function (result) {
    mongo.data.createCollection();
    methods.filterJSONBathgate(methods.filterJSON(result));
    mongo.data.retrieveData();
})
    .catch(function (err) {
        console.log('Error during print test: ' + err)
    });

// rejections are due to duplicate entries and missing service ids (not all services contain information)
process.on('unhandledRejection', (reason, p) => {
})

exports.data = methods;