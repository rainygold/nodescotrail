// imports and requires
const should = require('should');
const dbjs = require('../db');
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost/ScotRailJourneys';
const options = {
    useNewUrlParser: true,
}

// tests for db.js functions
describe('db.js Testing', function () {

    // test for inserting data into the database
    describe('insertDataIntoDb', function () {

        it('inserts data into the configured database (mongo)', function () {

            const testService = {
                id: 'edsfdeFESFESfsefsefSEFES',
                platform: '2',
                destination: 'Edinburgh',
                departs: '11:53',
                arrives: '12:00',
                expected: '12:01',
                origin: 'Glasgow Queen Street',
                operator: 'ScotRail'
            };

            // insert the fake data
            dbjs.data.insertData(testService);

            // check if the data exists in the database
            MongoClient.connect(url, options, function (err, db) {

                const findDocu = db.db().collection('Trains').find();
                findDocu.next().then(function (result, err) {
                    db.db().collection('Trains').drop().then(function () {
                        db.close();
                        result.should.have.keys('id', 'platform', 'destination');
                    });
                });
            });
        });
    });
})