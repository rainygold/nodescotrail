// imports and requires
const should = require('should');
const api = require('../api');

// tests for api.js functions
describe('api.js Testing', function () {

    // test for retrieving main json from the api 
    describe('GETJsonFromAPI', function () {

        it('returns json from an external api (scotrail-api)', function () {
            const jsonToTest = api.data.getJson();

            jsonToTest.then(function (result) {

                // checks the result for type
                should(result).be.an.Promise;

                // checks the result for json keys
                result.services[0].should.have.keys('id', 'platform', 'destination', 'departs',
                    'arrives', 'expected', 'origin', 'operator');
            });

        });
    });

    // test for retrieving service json from the api
    describe('GETServiceJsonFromAPI', function () {

        it('returns service json from an external api (scotrail-api)', function () {
            const jsonToTest = api.data.getJson();
            jsonToTest.then(function (result) {

                const serviceJsonToTest = api.data.getServiceJson(result.services[0].id);

                serviceJsonToTest.then(function (secondResult) {

                    // checks the result for json keys
                    secondResult[0].should.have.keys('Station');
                })
            })
        });
    });
})

