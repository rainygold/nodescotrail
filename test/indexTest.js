// imports and requires
const should = require('should');
const api = require('../api');
const index = require('../index');

// tests for index.js functions
describe('index.js Tests', function () {

    // retrieve the json required for testing purposes
    const testJson = api.data.getJson();

    describe('filterJSON', function () {


        it('returns filtered json according to a predicate', function () {

            testJson.then(function (result) {

                // filter the json
                const filteredJsonToTest = index.data.filterJSON(result);

                // check the json for predicate
                should(filteredJsonToTest[0]).have.value('destination', 'Edinburgh');
            })
        });
    });

    describe('filterJSONBathgate', function () {

        it('returns a second filtering json according to a secondary predicate', function () {

            testJson.then(function (result) {

                const testServiceJson = api.data.getServiceJson(result.services[0].id);

                testServiceJson.then(function (secondResult) {

                    // filter the json
                    const filteredServiceJson = index.data.filterJSONBathgate(secondResult);

                    // check the json for predicate
                    should(filteredServiceJson[0]).have.value('Station', 'Bathgate');
                });
            })

        })
    });
});